// Port to listen requests from
var port = 1234;



// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var cookie = require('cookie');

var md5 = require('md5');



// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

//db.run('CREATE TABLE sessions (token TEXT )');  

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});




// Serve static files
app.use(express.static('public'));





app.get("/users", function(req, res, next) {

	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);

	});
});




app.post("/login", function(req, res, next) {

	console.log(req.body.ID);
	console.log(req.body.MDP);

	//Verification du mdp et de l'id + cryptage du mot de passe
	db.get('SELECT ident,password FROM users WHERE ident=?  AND  password=? ;', [req.body.ID, md5(req.body.MDP)], function(err, rep) {
 	

		if( rep )
		{	

			//Generation du token
			var token= Math.random().toString(36).substr(2); 

			//cookie
			res.cookie('token', token);
			
			// étape 1 : je cherche si déjà une session dans la base
			db.get(' SELECT * FROM sessions WHERE  ident =? ;',[rep.ident], function(err, rep2) {

		

				if (rep2 ) {

					// si oui : étape 2 : je modifie l'enregistrement
					db.run('UPDATE sessions SET token=? WHERE ident=?;',[token,rep.ident],function(err) {
				
						if( !err )
						{	
							res.json({status: true});	
						}
						else
						{
							res.json({status: false});
						}
					});
				}
				else {

					// si non : étape 3 : je crée un enregistrement
					db.run('INSERT INTO sessions (ident,token) values (?,?);',[rep.ident,token], function(err) {

				
						if( !err )
						{	
							res.json({status: true,token :token});	
						}
						else
						{
							res.json({status: false});
						}
					});
				}
			});			
		}
		else
		{
			res.json({status: false});
		}
	});
});


// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});












